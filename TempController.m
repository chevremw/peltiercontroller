
classdef TempController < handle
    % This class control the Peltier temperature controller for the microscope
    properties(Access=private)
        m_serial
        m_strToParse
    end
    properties(GetAccess=public, SetAccess=private)
        lastData
    end
    properties(Access=public)
        callback
    end
    methods
        function obj = TempController(dev, speed)
            % Constructor.
            % Arg:  dev     COM port
            %       speed   Speed for the serial communication (default:
            %               115200)
            if nargin < 2
                speed = 115200;
            end
            obj.m_serial = serial(dev, 'BaudRate', speed);
            obj.m_serial.Terminator = 'LF';
            obj.m_serial.BytesAvailableFcn = @(~,~)(obj.parse());
            obj.m_strToParse = '';
            obj.callback = '';
            
            fopen(obj.m_serial);
        end
        function delete(obj)
            % Class destructor
            fclose(obj.m_serial);
        end
        function R = setpoint(obj, Tci)
            % Usage:    obj.setpoint(Tc)    Set the Setpoint to Tc
            ret = obj.command(sprintf('S%.3f', Tci), 1);
            
            if ~ret
                R = false;
                return;
            end
            
            reg = 'Temperature cible peltier = ([0-9]+\.[0-9]+)';
            tok = regexp(ret{1}, reg, 'tokens');
            if isempty(tok)
                R = false;
                return;
            else
                R = str2double(tok{1,1}{1,1}) == Tci;
            end
        end
        function pid = PID(obj, p,i,d)
            % Usage:    obj.PID()       Get the PID values
            %           obj.PID(p,i,d)  Set the PID values to p,i and d
            if nargin < 2
                ret = obj.command('P', 4);
            else
                if isscalar(p) && isnumeric(p) && ...
                        isscalar(i) && isnumeric(i) &&...
                        isscalar(d) && isnumeric(d)
                    ret = obj.command(sprintf('P%.3f,%.3f,%.3f',p,i,d),4);
                else
                    error('You must provide P,I,D as scalar numbers');
                end
            end
            
            if isempty(ret)
                pid = false;
                return;
            end
            
            reg1 = 'Kp = ([0-9]+(?:\.[0-9]*)?)';
            reg2 = 'Ki = ([0-9]+(?:\.[0-9]*)?)';
            reg3 = 'Kd = ([0-9]+(?:\.[0-9]*)?)';
            
            ok = false;
            
            tok = regexp(ret{2},reg1, 'tokens');
            if ~isempty(tok)
                pid.p = str2double(tok{1,1}{1,1});
                ok = true;
            end
            
            tok = regexp(ret{3},reg2, 'tokens');
            if ~isempty(tok)
                pid.i = str2double(tok{1,1}{1,1});
                ok = true;
            end
            
            tok = regexp(ret{4},reg3, 'tokens');
            if ~isempty(tok)
                pid.d = str2double(tok{1,1}{1,1});
                ok = true;
            end
            
            if ~ok
                pid = ok;
            end
        end
        function R = enablePeltier(obj, onoff)
            % Usage: obj.enablePeltier(onoff)   Enable or disable Peltier
            %           onoff: boolean
            
            if ~isscalar(onoff)
                error('onoff must be a scalar boolean');
            end
            onoff = bool(onoff);
            if onoff
                ret = obj.command('O', 1);
            else
                ret = obj.command('F', 1);
            end
            reg = 'Peltier (On|Off)';
            tok = regexp(ret, reg, 'tokens');
            if ~isempty(tok)
                R = true;
            else
                R = false;
            end
        end
        function cf = conversionFactorsPeltier(obj, off, slp)
            % Usage:    obj.conversionFactorsPeltier()
            %           Get the conversions factors
            %
            %           obj.conversionFactorsPeltier(offset, slope)
            %           Formula: T = slope*input + offset
            %           Set the conversion factor from Volt to °C
            
            if nargin < 2
                ret = obj.command('T', 2);
            else
                if isscalar(off) && isnumeric(off) && ...
                        isscalar(slp) && isnumeric(slp)
                    ret = obj.command(sprintf('T%.6f,%.6f',slp, off),2);
                else
                    error('You must provide offset, slope as scalar numbers');
                end
            end
            
            if isempty(ret)
                cf = false;
                return;
            end
            
            reg1 = 'Offset temperature peltier = (\-?[0-9]+\.[0-9]{6})';
            reg2 = 'Facteur temperature peltier = (\-?[0-9]+\.[0-9]{6})';
            
            ok = false;
            
            tok = regexp(ret{1},reg1, 'tokens');
            if ~isempty(tok)
                cf.offset = str2double(tok{1,1}{1,1});
                ok = true;
            end
            
            tok = regexp(ret{2},reg2, 'tokens');
            if ~isempty(tok)
                cf.slope = str2double(tok{1,1}{1,1});
                ok = true;
            end
            
            if ~ok
                cf = ok;
            end
        end
        function cf = conversionFactorsInternal(obj, off, slp)
            % Usage:    obj.conversionFactorsInternal()
            %           Get the conversions factors
            %
            %           obj.conversionFactorsInternal(offset, slope)
            %           Formula: T = slope*input + offset
            %           Set the conversion factor from Volt to °C
            
            if nargin < 2
                ret = obj.command('t', 2);
            else
                if isscalar(off) && isnumeric(off) && ...
                        isscalar(slp) && isnumeric(slp)
                    ret = obj.command(sprintf('t%.6f,%.6f',slp, off),2);
                else
                    error('You must provide offset, slope as scalar numbers');
                end
            end
            
            if isempty(ret)
                cf = false;
                return;
            end
            
            reg1 = 'Offset temperature interne = (\-?[0-9]+\.[0-9]{6})';
            reg2 = 'Facteur temperature interne = (\-?[0-9]+\.[0-9]{6})';
            
            ok = false;
            
            tok = regexp(ret{1},reg1, 'tokens');
            if ~isempty(tok)
                cf.offset = str2double(tok{1,1}{1,1});
                ok = true;
            end
            
            tok = regexp(ret{2},reg2, 'tokens');
            if ~isempty(tok)
                cf.slope = str2double(tok{1,1}{1,1});
                ok = true;
            end
            
            if ~ok
                cf = ok;
            end
        end
    end
    methods(Access=private)
        function R = command(obj, cmd, nbr)
            % Input commands + recover results
            obj.m_strToParse = {};
            fprintf(obj.m_serial,'%s\n', cmd);
            i = 0; % 10 sec timeout
            while length(obj.m_strToParse) <= nbr && i < 100
                pause(0.1);
                i = i+1;
            end
            R = obj.m_strToParse(2:end);
            if i == 100
                R = {};
            end
        end
        function parse(obj)
            % Handle the parsing of chains from the controller
            l = fgetl(obj.m_serial);
            reg = 'Interne ([+\-]?[0-9]+\.[0-9]{2}) \/ Peltier : ([+\-]?[0-9]+\.[0-9]{3}) \/ ([0-9]+\.[0-9]{2}) => Consigne Peltier=([+\-]?[0-9]+\.[0-9]{2}) : I1= ([0-9]+) : I2= ([0-9]+)';
            tok = regexp(l, reg, 'tokens');
            
            if ~isempty(tok)
                data = str2double(tok{1,1}(1,:));
                s.TInterne = data(1);
                s.TPeltier = data(2);
                s.TConsigne = data(3);
                s.PWM = data(4);
                s.I1 = data(5);
                s.I2 = data(6);
                obj.lastData = s;
                if ~isempty(obj.callback)
                    obj.callback(s);
                end
                return
            else
                obj.m_strToParse{end+1} = l;
            end
        end
    end
end

