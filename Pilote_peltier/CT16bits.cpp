#include "CT16bits.h"

float CT16bits::getTemperature(){
  int16_t result;
  startConversion();
  getResult(&result);
  float val = result/10;
 return val;
}
