#include "Arduino.h"
#include "Driver_Moteur.h"

DRIVERMOTEUR::DRIVERMOTEUR(int In1, int In2, int Inh1, int Inh2){
  _IN_1=In1;
  _IN_2=In2;
  _INH_1=Inh1;
  _INH_2=Inh2;
  _HUsed=_IN_1;
  _Commande=0;
  pinMode (_IN_1 , OUTPUT);      //pin PID Enable
  pinMode (_IN_2 , OUTPUT);      //pin sens 1 PID
  pinMode (_INH_1, OUTPUT);      //pin sens 2 PID
  pinMode (_INH_2, OUTPUT);      //pin sens 2 PID
  Commande(0);
  off();
  up();
  }

void DRIVERMOTEUR::Commande(int Niveau){
  if (Niveau<0) Niveau =0;
  if (Niveau>255) Niveau =255;
  _Commande=Niveau;
  digitalWrite (_IN_1, LOW);
  digitalWrite (_IN_2, LOW);
   analogWrite(_HUsed, _Commande);
  }
  
void DRIVERMOTEUR::off(){
  digitalWrite (_IN_1, LOW);
  digitalWrite (_IN_2, LOW);
  digitalWrite (_INH_1, LOW);
  digitalWrite (_INH_2, LOW);
  }
  
void DRIVERMOTEUR::on(){  
  digitalWrite (_INH_1, HIGH);
  digitalWrite (_INH_2, HIGH);
  Commande(_Commande);
  }
  
void DRIVERMOTEUR::up(){
  _HUsed=_IN_2;
  }
  
void DRIVERMOTEUR::down(){
  _HUsed=_IN_1;
  }
