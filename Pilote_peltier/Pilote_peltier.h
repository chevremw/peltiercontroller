#ifndef _PILOTEPELTIER_
#define _PILOTEPELTIER_

#include <PID_v1.h>
#include <Wire.h>
#include <MCP342X.h>
#include "Driver_Moteur.h"

#define VERSION 0.91

#define adresseTemp 0x6E

//Digitals output
#define SortieVentilateur 8
#define IN_1  3
#define IN_2  11
#define INH_1 12
#define INH_2 13

//Analogiques input
#define IS_1 A0
#define IS_2 A1
#define EntreeTemp A2

float FacteurTemp = 1.0/491.0;
float OffsetTemp = 0.0;
float FTemp = 0.18;
float OTemp = 0;
float TempSeuil = 23;
float tempInterne;
bool Vent_Auto = false;
bool VState = false;

double PID_KP = 1;
double PID_KI = 0;
double PID_KD = 0;
double TempPeltier,ConsignePeltier;
double Setpoint = 24;
bool Peltier_On = false;

unsigned long timermesure = 0;
unsigned long currentTime;
unsigned long delaiAffichage = 2000;
bool AState = true;

unsigned long lastVentilateurOn = 0;
unsigned long tempRefroidir = 1000*15; //une minute

#define BAUDRATE 115200

String cmd = String("");                   //chaine recu sur le port serie
boolean stringComplete = false;            //etat de la chaine sur le port serie
float Valeurs[5];


MCP342X myADC(adresseTemp);
PID myPID(&TempPeltier, &ConsignePeltier, &Setpoint,PID_KP,PID_KI,PID_KD, DIRECT);
DRIVERMOTEUR myDriver(IN_1,IN_2,INH_1,INH_2);

union floatuint8union {
  float f;
  uint8_t b[4];
};

#define EE_MAGIC 0x00
#define EE_PID_P 0x04
#define EE_PID_I 0x08
#define EE_PID_D 0x0C
#define EE_TINT_OFF 0x10
#define EE_TINT_SLP 0x14
#define EE_TPLT_OFF 0x18
#define EE_TPLT_SLP 0x1C

#endif
