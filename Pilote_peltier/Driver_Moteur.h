#ifndef _DRIVERMOTEUR_
#define _DRIVERMOTEUR_
#include "Arduino.h"
class DRIVERMOTEUR {
  private:
    int _IN_1;
    int _IN_2;
    int _INH_1;
    int _INH_2;
    int _Commande;
    int _HUsed;
  public:
   DRIVERMOTEUR(int In1, int In2, int Inh1, int Inh2);
   void Commande(int Niveau);
   void off();
   void on();
   void up();
   void down();
  };
  
#endif
