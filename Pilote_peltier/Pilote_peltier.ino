#include "Pilote_peltier.h"

#include <PID_v1.h>
#include <Wire.h>
#include <MCP342X.h>
#include <EEPROM.h>
#include "Driver_Moteur.h"


/************************************************************************/
/************************************************************************/
/************************************************************************/
void setup() {
  Wire.begin();  // join I2C bus
  TWBR = 12; // 400 kHz (maximum)

  Serial.begin(BAUDRATE);
  cmd.reserve(200); 
  Serial.print(F("Pilotage pret V:"));
  Serial.println(VERSION);
  
  pinMode(SortieVentilateur, OUTPUT);

  myDriver.Commande(0);
  myDriver.off();
  myDriver.up();

  Serial.println(F("Test connection MCP342X ..."));
  Serial.println(myADC.testConnection() ? F("MCP342X connection ok") : F("MCP342X connection echouee"));
    
  myADC.configure( MCP342X_MODE_CONTINUOUS |
                   MCP342X_CHANNEL_1 |
                   MCP342X_SIZE_16BIT |
                   MCP342X_GAIN_1X
                 );
  Setpoint=20;
  myPID.SetOutputLimits(-255, 255);
  myPID.SetMode(AUTOMATIC);

  // Turn on ventigrad
  VOn();
  PID_KP = readFloatFromEEPROM(EE_PID_P);
  PID_KI = readFloatFromEEPROM(EE_PID_I);
  PID_KD = readFloatFromEEPROM(EE_PID_D);
  
  myPID.SetTunings(PID_KP,PID_KI,PID_KD);

  FacteurTemp = readFloatFromEEPROM(EE_TPLT_SLP);
  OffsetTemp  = readFloatFromEEPROM(EE_TPLT_OFF);  
  FTemp = readFloatFromEEPROM(EE_TINT_SLP);
  OTemp  = readFloatFromEEPROM(EE_TINT_OFF);
  
}

void storeFloatToEEPROM(int address, float val)
{
  floatuint8union tmp;
  tmp.f = val;
  for(int i(0);i<4;i++) EEPROM.update(address+i,tmp.b[i]);
}

float readFloatFromEEPROM(int address)
{
  floatuint8union tmp;
  for(int i(0);i<4;i++)  tmp.b[i] = EEPROM.read(address+i);
  return tmp.f;
}

/************************************************************************/
/************************************************************************/
/************************************************************************/
void loop() {
  currentTime = millis();
   
  DoPID();                //acquis la temperature Peltier et fait le PID
  
  DoTempInterne();        //acquis la temperature interne et refroidi si necessaire
                    
  DoAffichage();          //affiche les infos de temperature, commande, courant utilise ...
 
}


//-----------------------------------------------------------------------------
//-------- SOUS PROGRAMMES ----------------------------------------------------
//-----------------------------------------------------------------------------
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read(); //lit le port serie
    cmd += inChar;                     //l'ajoute a la commande
    if (inChar == '\n') {              //si retour chariot recu alors on traite la commande
      ProcessCmd();
      clearCmd();
    }
  }
}

 // Réinitialiser le buffer (et vider le port série)
  void clearCmd(){
    cmd="";
    Serial.flush();
  }
  
void Interligne(){
       Serial.println(F("--------------------------------"));
}

/************************************************************************/
/************************************************************************/
/************************************************************************/
 void ProcessCmd(){
   int cc = cmd[0];             //le caractere de commande est le premier de la chaine de caractere recu
   String val=cmd.substring(1); //les parametres sont apres le 1er caractere
   cmd.trim();
   Serial.print(F("Cmd > "));
   Serial.println(cmd);
   int n=0;
   int i=0;
   do{
      int commaIndex = cmd.indexOf(',',n+1);
      String Value = cmd.substring(n+1, commaIndex);
      n=commaIndex;
      if (Value!=""){
                     Valeurs[i]=Value.toFloat();
                     i++;
                    }
   }while(n>=0);
  
   switch (cc){
    case '?' : //aide
               Interligne();
               Serial.print(F("Version : "));
               Serial.println(VERSION);
               Serial.println(F("Liste des commandes :"));
               Serial.println(F("?    : Cet aide"));      
               Serial.println(F("Txxx,yyy : Temperature peltier facteur,offset"));
               Serial.println(F("txxx,yyy : Temperature interne facteur,offset"));
               Serial.println(F("Sxxx     : Seuil de temperature peltier"));
               Serial.println(F("sxxx     : Seuil de temperature interne"));
               Serial.println(F("A        : ventilateur automatique"));
               Serial.println(F("V        : demarre le ventilateur"));
               Serial.println(F("B        : stop le ventilateur"));
               Serial.println(F("D        : delai entre 2 affichages de temperature=xxx ms"));
               Serial.println(F("Pxx,yy,zz: Parametres PID"));
               Serial.println(F("O        : Peltier On"));
               Serial.println(F("F        : Peltier Off"));
               Serial.println(F("M        : renvoi Peltier"));
               Serial.println(F("a        : Peltier on"));
               Serial.println(F("q        : Peltier off"));
               Serial.println(F("c        : Peltier up"));
               Serial.println(F("v        : Peltier down"));
               
               Interligne();
              break;
     case 'A' : Vent_Auto=!Vent_Auto;
                Serial.print(F("Ventilateur "));                
                Serial.print(Vent_Auto?F(""):F("non "));     
                Serial.println(F("automatique"));
                break;
     case 'M' :
                Serial.print(tempInterne,2);
                Serial.print(F(" : "));
                Serial.println(TempPeltier,3);
                Serial.print(F(" / "));
                Serial.print(Setpoint);
                Serial.print (F(" => Consigne Peltier="));
                Serial.println(ConsignePeltier,2);
                break;
     case 'V' :
               VOn();
               break;  
     case 'B' :
               VOff();
               break; 
     case 'T' :
               if (i!=0) {
                          OffsetTemp=Valeurs[1];
                          FacteurTemp=Valeurs[0];
                          storeFloatToEEPROM(EE_TPLT_OFF, OffsetTemp);
                          storeFloatToEEPROM(EE_TPLT_SLP, FacteurTemp);
                         }
               Serial.print(F("Offset temperature peltier = "));
               Serial.println(OffsetTemp,6);
               Serial.print(F("Facteur temperature peltier = "));
               Serial.println(FacteurTemp,6);
               break;  
     case 't' :
               if (i!=0) {
                           OTemp=Valeurs[1];
                           FTemp=Valeurs[0];
                           storeFloatToEEPROM(EE_TINT_OFF, OTemp);
                           storeFloatToEEPROM(EE_TINT_SLP, FTemp);
                         }
               Serial.print(F("Offset temperature interne = "));
               Serial.println(OTemp,6);
               Serial.print(F("Facteur temperature interne = "));
               Serial.println(FTemp,6);
               break;  
     case 'S' :
               if (i!=0) { Setpoint=Valeurs[0];}
               Serial.print(F("Temperature cible peltier = "));
               Serial.println(Setpoint);
               break;  
     case 's' :
               if (i!=0) { TempSeuil=Valeurs[0];}
               Serial.print(F("Temperature interne seuil = "));
               Serial.println(TempSeuil);
               break;  
     case 'D' :
               if (i!=0) {
                          if (Valeurs[0]>=100 && Valeurs[0]<=5000) delaiAffichage=Valeurs[0];
                          AState = (Valeurs[0] != 0);
                         }
               Serial.print(F("Delai d'affichage ="));
               Serial.println(delaiAffichage);
               break;  
     case 'P' :
               if (i!=0) {
                           PID_KP=Valeurs[0];
                           PID_KI=Valeurs[1];
                           PID_KD=Valeurs[2];
                           myPID.SetTunings(PID_KP, PID_KI, PID_KD);
                           
                           storeFloatToEEPROM(EE_PID_P, PID_KP);
                           storeFloatToEEPROM(EE_PID_I, PID_KI);
                           storeFloatToEEPROM(EE_PID_D, PID_KD);
                         }
               Serial.println(F("Parametres PID :"));
               Serial.print(F("Kp = "));
               Serial.println(PID_KP);
               Serial.print(F("Ki = "));
               Serial.println(PID_KI);
               Serial.print(F("Kd = "));
               Serial.println(PID_KD);
               break;                 
     case 'O' :
               Peltier_On=true;
               myDriver.on();
               Serial.println(F("Peltier On"));
               break;  
     case 'F' :
               Peltier_On=false;
               myDriver.off();
               Serial.println(F("Peltier Off"));
               break;  
     case 'a' :
               myDriver.on();
               Serial.println(F("On"));
               break;
     case 'q' :
               myDriver.off();
               Serial.println(F("off"));
               break;
     case 'c' :
               myDriver.Commande(255);
               myDriver.up();
               Serial.println(F("up"));
               break;
     case 'v' :
               myDriver.Commande(255);
               myDriver.down();
               Serial.println(F("down"));
               break;
               
               
   }
 }   

/************************************************************************/
/************************************************************************/
/************************************************************************/
 float MesureTempPeltier(){
  static int16_t  result;
  
  myADC.startConversion();
  myADC.getResult(&result);
  return result*2*FacteurTemp + OffsetTemp;
  }

void VOn(){
 Serial.println(F("Ventilateur On"));
 digitalWrite(SortieVentilateur,HIGH);
 VState=true;
}

/***********************************************************************/
void VOff(){
 Serial.println(F("Ventilateur Off"));
 digitalWrite(SortieVentilateur,LOW);
 VState=false;
}

/***********************************************************************/
void DoTempInterne(){
 tempInterne = (float)analogRead(EntreeTemp)*FTemp+OTemp;
 if (Vent_Auto){
  if ((tempInterne>TempSeuil)){
                                lastVentilateurOn= currentTime;
                                if (!VState) VOn();
                              }
                         else {
                               if ((currentTime-lastVentilateurOn>=tempRefroidir) && VState) VOff();
                              }
 }          
}

/*************************************************************************/
void DoAffichage(){
  
  if ((currentTime-timermesure >=delaiAffichage)&&AState){
    timermesure=currentTime;  
    float intensite1 = (float)analogRead(IS_1); // Pourquoi utiliser float plutot que uint16_t?? (aucuns calculs effectuées + sortie sans virgule...)
    float intensite2 = (float)analogRead(IS_2);
    Serial.print(F("Interne "));
    Serial.print(tempInterne,2);
    Serial.print(F(" / Peltier : "));
    Serial.print(TempPeltier,3);
    Serial.print(F(" / "));
    Serial.print(Setpoint);
    Serial.print (F(" => Consigne Peltier="));
    Serial.print(ConsignePeltier,2);
    Serial.print(F(" : I1= "));
    Serial.print(intensite1,0);
    Serial.print(F(" : I2= "));
    Serial.println(intensite2,0);
  }
}
/************************************************************************/
/************************************************************************/
/************************************************************************/
void DoPID(){
  TempPeltier = MesureTempPeltier();
  myPID.Compute();
  if (Peltier_On){
    if (ConsignePeltier>0){
                            //digitalWrite(IN_1,0);
                            //analogWrite(IN_2,ConsignePeltier); //sens 2
                            myDriver.Commande(ConsignePeltier);
                            myDriver.up();
                           }
                     else { 
                            //digitalWrite(IN_2,0);
                            //analogWrite(IN_1,ConsignePeltier); //sens 1
                            myDriver.Commande(-ConsignePeltier);
                            myDriver.down();
                          }
    
    }
   // else reset_ports();
}
