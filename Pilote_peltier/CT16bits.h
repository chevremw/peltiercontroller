#ifndef _CT16B_
#define _CT16B_

#include <MCP342X.h>

class CT16bits: public MCP342X
{
  public:
   float getTemperature();
};

#endif
